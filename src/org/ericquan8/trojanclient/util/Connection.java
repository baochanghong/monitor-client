/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanclient.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.ericquan8.trojanclient.Key;

/**
 *
 * @author eric
 */
public class Connection {
    
    private static Socket socket = null;
    private static ObjectInputStream reader = null;
    private static ObjectOutputStream writer = null;
    
    
    public static void init(){
        try {
            socket = new Socket(Key.host, Key.port);
            reader = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            writer = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        } catch (UnknownHostException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void write(Object object){
    	//todo...
    }
    
    public static Object read(){
        return null;
    }
    
}
