/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanclient.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author eric
 */
public class FileUtil {
    
    public static void downloadFile(byte[] b, String fileName) throws IOException {

        long k = 0;

        File targetFile = new File(fileName);

        if(!targetFile.getParentFile().exists()){
            targetFile.getParentFile().mkdir();
        }
        if (targetFile.exists()) {
            targetFile.delete();
        }

        FileOutputStream out = new FileOutputStream(fileName);
        out.write(b, 0, b.length);
        out.flush();
        out.close();
    }
    
}
