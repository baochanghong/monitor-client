/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanclient.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Properties;

/**
 *
 * @author eric
 */
public class PropertiesReader {

    public static Properties read(File properties) throws IOException {
        Properties pro = new Properties();
        pro.load(new InputStreamReader(new FileInputStream(properties), "UTF-8"));
        return pro;
    }

    public static Iterator getProperties(File properties) throws IOException {
        Properties pro = new Properties();
        pro.load(new InputStreamReader(new FileInputStream(properties), "UTF-8"));
        return pro.stringPropertyNames().iterator();
    }
}
